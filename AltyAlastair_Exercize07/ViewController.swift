//
//  ViewController.swift
//  AltyAlastair_Exercize07
//
//  Created by Alastair Alty on 11/11/20.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    //MARK: Properties
    
    @IBOutlet weak var txtField1: UITextField!
    @IBOutlet weak var txtField2: UITextField!
    @IBOutlet weak var txtField3: UITextField!
    @IBOutlet weak var txtField4: UITextField!
    @IBOutlet weak var txtField5: UITextField!
    @IBOutlet weak var txtField6: UITextField!
    @IBOutlet weak var returnedStringLabel: UILabel!
    
    
    // Variable to hold the returned string
    var reducedString: String?
    // create an empty string array
    var arrayOfStrings: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        // Handle the text field’s user input through delegate callbacks.
        txtField1.delegate = self
        txtField2.delegate = self
        txtField3.delegate = self
        txtField4.delegate = self
        txtField5.delegate = self
        txtField6.delegate = self
    }
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        if textField == txtField1 { // Switch focus to other text field
                txtField2.becomeFirstResponder()
        }
        if textField == txtField2 { // Switch focus to other text field
            txtField3.becomeFirstResponder()
        }
        if textField == txtField3 { // Switch focus to other text field
            txtField4.becomeFirstResponder()
        }
        if textField == txtField4 { // Switch focus to other text field
            txtField5.becomeFirstResponder()
        }
        if textField == txtField5 { // Switch focus to other text field
            txtField6.becomeFirstResponder()
        }
        return true
  }

    //MARK: Functions
        func addToString(_ txtField: UITextField)  {
        if txtField1.hasText && txtField2.hasText && txtField3.hasText && txtField4.hasText && txtField5.hasText && txtField6.hasText {
            // Add to the array
            arrayOfStrings = [txtField1.text!, txtField2.text!, txtField3.text!, txtField4.text!, txtField5.text!, txtField6.text!]
        }
        else{
            // Create a new alert
            let dialogMessage = UIAlertController(title: "Attention", message: "You must fill in all of the text fields.", preferredStyle: .alert)
            dialogMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            // Present alert to user
            self.present(dialogMessage, animated: true, completion: nil)
        }
    }
    @IBAction func NextPage(_ sender: UIButton){
        self.performSegue(withIdentifier: "Next", sender: self)
    }
    // prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Next" {
            let destination = segue.destination as! StringDisplay
            addToString(txtField1)
            // set the string variables on stringDisplay to the txtField on viewController
            destination.string1 = txtField1.text!
            destination.string2 = txtField2.text!
            destination.string3 = txtField3.text!
            destination.string4 = txtField4.text!
            destination.string5 = txtField5.text!
            destination.string6 = txtField6.text!
        }
    }
    // unwind segue
    @IBAction func UnwindToViewController(_sender: UIStoryboardSegue){
        // set the label text to the variable declared above (reduced string)
        returnedStringLabel.text = reducedString!
    }
}

