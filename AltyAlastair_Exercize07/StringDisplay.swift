//
//  StringDisplay.swift
//  AltyAlastair_Exercize07
//
//  Created by Alastair Alty on 11/12/20.
//

import UIKit

class StringDisplay: UIViewController {
    @IBOutlet weak var stringLbl1: UILabel!
    @IBOutlet weak var stringLbl2: UILabel!
    @IBOutlet weak var stringLbl3: UILabel!
    @IBOutlet weak var stringLbl4: UILabel!
    @IBOutlet weak var stringLbl5: UILabel!
    @IBOutlet weak var stringLbl6: UILabel!
    
    
    
    var string1 = ""
    var string2 = ""
    var string3 = ""
    var string4 = ""
    var string5 = ""
    var string6 = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        stringLbl1.text = string1
        stringLbl2.text = string2
        stringLbl3.text = string3
        stringLbl4.text = string4
        stringLbl5.text = string5
        stringLbl6.text = string6
    }
    

    
    // MARK: - Navigation
    //MARK: Functions
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ReduceStringArray"{
            let destination = segue.destination as! ViewController
            destination.reducedString = destination.arrayOfStrings.reduce("", +)
    }
    }
     
}

